import { Injectable } from '@nestjs/common';
import * as Sequelize from 'sequelize'; // 引入 Sequelize 库
import sequelize from '../../database/sequelize'; // 引入 Sequelize 实例

import { makeSalt, encryptPassword } from '../../utils/cryptogram'; // 引入加密函数

@Injectable()
export class UserService {
  /**
   * 查询是否有该用户
   * @param username 用户名
   */
  async findOne(username: string): Promise<any | undefined> {
    const sql = `
    SELECT
      *
    FROM
      user
    WHERE
      username = '${username}'
  `; // 一段平淡无奇的 SQL 查询语句
    try {
      const user = (
        await sequelize.query(sql, {
          type: Sequelize.QueryTypes.SELECT, // 查询方式
          raw: true, // 是否使用数组组装的方式展示结果
          logging: false, // 是否将 SQL 语句打印到控制台
        })
      )[0];
      // 若查不到用户，则 user === undefined
      return user;
    } catch (error) {
      console.error(error);
      return void 0;
    }
  }

  /**
   * 注册
   * @param requestBody 请求体
   */
  async register(requestBody: any): Promise<any> {
    const { username, password } = requestBody;
    if (!username) {
      return {
        code: 400,
        msg: '用户名称不能为空',
      };
    }
    if (!password) {
      return {
        code: 400,
        msg: '用户密码不能为空',
      };
    }
    const user = await this.findOne(username);
    if (user) {
      return {
        code: 400,
        msg: '用户已存在',
      };
    }
    const passwd_salt = makeSalt(); // 制作密码盐
    const passwd = encryptPassword(password, passwd_salt); // 加密密码
    const registerSQL = `
      INSERT INTO user
        (username, passwd, passwd_salt)
      VALUES
        ('${username}', '${passwd}', '${passwd_salt}')
    `;
    try {
      await sequelize.query(registerSQL, { logging: false });
      return {
        code: 200,
        msg: 'Success',
      };
    } catch (error) {
      return {
        code: 500,
        msg: `Service error: ${error}`,
      };
    }
  }

  async list(): Promise<any> {
    const listSQL = `
    SELECT
      user_id, username, update_time  
    FROM
      user
    `;
    try {
      const user = await sequelize.query(listSQL, {
        type: Sequelize.QueryTypes.SELECT, // 查询方式
        raw: true, // 是否使用数组组装的方式展示结果
        logging: false,
      });
      console.log(user);
      return {
        code: 200,
        user,
        msg: 'Success',
      };
    } catch (error) {
      return {
        code: 500,
        msg: `Service error: ${error}`,
      };
    }
  }

  /**
   * 删除该用户
   * @param requestBody 用户名
   */

  async remove(requestBody: any): Promise<any> {
    const { username } = requestBody;
    if (!username) {
      return {
        code: 400,
        msg: '用户名称不能为空',
      };
    }
    if (username === 'admin') {
      return {
        code: 400,
        msg: 'admin用户不能删除',
      };
    }
    const user = await this.findOne(username);
    if (!user) {
      return {
        code: 400,
        msg: '该用户不存在',
      };
    }
    const sql = `
      DELETE FROM user WHERE username = '${username}';
    `;
    try {
      await sequelize.query(sql, {
        logging: false,
      });
      return {
        code: 200,
        msg: '删除成功',
      };
    } catch (error) {
      return {
        code: 500,
        msg: `Service error: ${error}`,
      };
    }
  }
}
