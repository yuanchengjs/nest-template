## 1.项目启动

- 安装依赖：cnpm i
- 项目启动：npm run start:dev
- 项目打包：npm run build

## 2.添加 url 前缀

```js
// main.ts
app.setGlobalPrefix('xxx');

// app.controller.ts
@Controller('xxx')
@Post('xxx')
@Get('xxx')
```

## 3.使用 swagger

- 安装依赖：cnpm i --save @nestjs/swagger swagger-ui-express
- 文档地址：http://localhost:3000/api-docs/

## 4.nest 项目部署

- 直接拷贝 dist 文件夹通过 node main.js 启动报错，需要拷贝整个项目
- 安装 pm2：npm i -g pm2
- 进入 dist 文件夹使用 pm2 启动：pm2 start main.js
- 线上文档地址：http://116.62.124.208:3000/api-docs/

## 5.购买阿里云数据库

- 可设置白名单：0.0.0.0/0
- 使用 mysql workbench 可远程连接云数据库

```js
port: 3306,
host: 'rm-bp16g8j1o2ey3a90yfo.mysql.rds.aliyuncs.com',
user: 'yuancheng',
password: 'w1582682448W',
database: 'nest_test', // 库名
```
